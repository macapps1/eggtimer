//
//  ViewController.swift
//  EggTimer
//
//  Created by mohammed-7418 on 14/08/18.
//  Copyright © 2018 mohammed-7418. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var EggImage: NSImageView!
    @IBOutlet weak var TimeLabel: NSTextField!
    
    @IBOutlet weak var StartButton: NSButton!
    @IBOutlet weak var StopButton: NSButton!
    @IBOutlet weak var ResetButton: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    
    @IBAction func StartButtonClicked(_ sender: Any) {
    }
       
    @IBAction func StopButtonClicked(_ sender: Any) {
    }
    
    @IBAction func ResetButtonClicked(_ sender: Any) {
    }
    
    @IBAction func startMenuItemSelected(_ sender: Any){
        StartButtonClicked(sender)
    }
    
    @IBAction func stopMenuItemSelected(_ sender: Any){
        StopButtonClicked(sender)
    }
    
    @IBAction func resetMenuItemSelected(_ sender: Any){
        ResetButtonClicked(sender)
    }
}

