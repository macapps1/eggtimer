//
//  PreferencesViewController.swift
//  EggTimer
//
//  Created by mohammed-7418 on 16/08/18.
//  Copyright © 2018 mohammed-7418. All rights reserved.
//

import Cocoa

class PreferencesViewController: NSViewController {
    @IBOutlet weak var TimerLabel: NSTextField!
    @IBOutlet weak var TimerSlider: NSSlider!
    @IBOutlet weak var PresetPopup: NSPopUpButton!
    
    @IBAction func PopupChanged(_ sender: NSPopUpButton) {
    }
    @IBAction func SliderChanged(_ sender: NSSlider) {
    }
    @IBAction func OkClicked(_ sender: Any) {
    }
    @IBAction func CancelClicked(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
